﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class SpellComponentsManager : MonoBehaviour
    {
        private static SpellComponentsManager instance;

        public GameObject abilityButton;

        [Header ( "Indicators" )]
        public Indicator areaIndicator;
        public Indicator rangeIndicator;
        public Indicator minRangeIndicator;
        public Indicator coneIndicator;
        public Indicator circleIndicator;
        public Indicator lineIndicator;
        public Indicator lineRenderer;
        public Indicator curvedine;
        public Indicator simpleLine;
        public Indicator sphereCollider;

        void Awake ()
        {
            if ( !SpellComponentsManager.instance )
            {
                SpellComponentsManager.instance = this;
            }
            else
                Destroy ( this );
        }

        public static SpellComponentsManager getInstance ()
        {
            if ( !SpellComponentsManager.instance )
                SpellComponentsManager.instance = new SpellComponentsManager ();
            return SpellComponentsManager.instance;
        }

        public void instantiateIndicators ()
        {
            // ----- Instantiating Indicators ----- //
            areaIndicator = Instantiate ( areaIndicator , transform );
            rangeIndicator = Instantiate ( rangeIndicator , transform );
            minRangeIndicator = Instantiate ( rangeIndicator , transform );
            coneIndicator = Instantiate ( coneIndicator , transform );
            circleIndicator = Instantiate ( circleIndicator , transform );
            lineIndicator = Instantiate ( lineIndicator , transform );
            lineRenderer = Instantiate ( lineRenderer , transform );
            curvedine = Instantiate ( curvedine , transform );
            simpleLine = Instantiate ( simpleLine , transform );
            sphereCollider = Instantiate ( sphereCollider , transform );
        }

        public Indicator getIndicator ( SpellMetrics.spellIndicators indicator )
        {
            switch ( indicator )
            {
                case SpellMetrics.spellIndicators.Null:
                    return null;
                case SpellMetrics.spellIndicators.Area:
                    return areaIndicator;
                case SpellMetrics.spellIndicators.Range:
                    return rangeIndicator;
                case SpellMetrics.spellIndicators.MinRange:
                    return minRangeIndicator;
                case SpellMetrics.spellIndicators.Cone:
                    return coneIndicator;
                case SpellMetrics.spellIndicators.Circle:
                    return circleIndicator;
                case SpellMetrics.spellIndicators.Line:
                    return lineIndicator;
                case SpellMetrics.spellIndicators.LineRenderer:
                    return lineRenderer;
                case SpellMetrics.spellIndicators.CurvedLine:
                    return curvedine;
                case SpellMetrics.spellIndicators.SimpleLine:
                    return simpleLine;
                case SpellMetrics.spellIndicators.SphereCollider:
                    return sphereCollider;
            }

            return null;
        }
    }
}