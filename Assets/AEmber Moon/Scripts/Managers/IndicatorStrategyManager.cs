﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class IndicatorStrategyManager : MonoBehaviour
    {
        public IndicatorStrategy indicator;
        private static IndicatorStrategyManager instance;

        void Awake ()
        {
            if ( !IndicatorStrategyManager.instance )
            {
                IndicatorStrategyManager.instance = this;
            }
            else
                Destroy ( gameObject );
        }

        public static IndicatorStrategyManager getInstance ()
        {
            if ( !IndicatorStrategyManager.instance )
                IndicatorStrategyManager.instance = new IndicatorStrategyManager ();
            return IndicatorStrategyManager.instance;
        }

        private void Update ()
        {
            if ( indicator )
            {
                indicator.update ();
                if ( Input.GetMouseButtonDown ( 0 ) )
                    indicator.clicGauche ();
                if ( Input.GetMouseButtonDown ( 1 ) )
                    indicator.clicDroit ();
            }
        }

        public void setIndicator ( IndicatorStrategy indicator , GenericAbility spell )
        {
            this.indicator = indicator;

            indicator.spell = spell;
            indicator.startUnhide ();
            indicator.setUp ();
        }

        public void unsetIndicator ()
        {
            if ( indicator )
                indicator.unsetIndicator ();
            indicator = null;
        }
    }
}