﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class WorldManager : MonoBehaviour
    {
        public static Vector3 mouseWorldPos = new Vector3 ();
        public GameObject groundPlane;

        private static WorldManager instance;

        void Awake ()
        {
            if ( !WorldManager.instance )
            {
                WorldManager.instance = this;
            }
            else
                DestroyImmediate ( this );
        }

        public static WorldManager getInstance ()
        {
            if ( !WorldManager.instance )
                WorldManager.instance = new WorldManager ();
            return WorldManager.instance;
        }

        private void Update ()
        {
            updateWorldPos ();
        }

        private void updateWorldPos ()
        {
            Ray ray = Camera.main.ScreenPointToRay ( Input.mousePosition );
            RaycastHit hit;
            if ( groundPlane.GetComponent<Collider> ().Raycast ( ray , out hit , 10000.0F ) )
            {
                mouseWorldPos = hit.point;
            }
        }
    }
}