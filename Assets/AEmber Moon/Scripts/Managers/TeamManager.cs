﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class TeamManager : MonoBehaviour
    {
        private static TeamManager instance;

        [System.Serializable]
        protected class teamMember
        {
            public Character perso;
            public int teamNumber;
        }
        [Header ( "Participants" )]
        [SerializeField]
        protected List<teamMember> participants;
        private Dictionary<int , List<Character>> teams = new Dictionary<int , List<Character>> ();

        public Character persoActif;

        private void Start ()
        {
            SpellComponentsManager.getInstance ().instantiateIndicators ();

            StartCoroutine ( setUpTeams () );
        }

        void Awake ()
        {
            if ( !TeamManager.instance )
            {
                TeamManager.instance = this;
            }
            else
                DestroyImmediate ( this );
        }

        public static TeamManager getInstance ()
        {
            if ( !TeamManager.instance )
                TeamManager.instance = new TeamManager ();
            return TeamManager.instance;
        }

        public IEnumerator setUpTeams ()
        {
            foreach ( teamMember perso in participants )
            {
                if ( !teams.ContainsKey ( perso.teamNumber ) )
                    teams.Add ( perso.teamNumber , new List<Character> () );
                teams[ perso.teamNumber ].Add ( perso.perso );
                StartCoroutine ( perso.perso.initCharacter () );
            }

            persoActif = participants[ 0 ].perso;

            yield return null;
        }

        public bool compareTeam ( Character first , Character second )
        {
            foreach ( KeyValuePair<int , List<Character>> team in teams )
                if ( team.Value.Contains ( first ) )
                    return team.Value.Contains ( second );

            return false;
        }
    }
}