﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Æmber_Moon
{
    public class Character : MonoBehaviour, IComparable
    {
        [Header ( "Character" )]
        public string characterName;
        public float characterRadius = 0.75f;
        public GameObject characterCircle;
        public CharacterInfos characterInfos;

        [Header ( "Stats de base" )]
        public int actionPoints;

        [Header ( "Stats Offensives" )]
        public Damage[] damages;

        [Header ( "Stats Défensives" )]
        public Damage[] resistances;

        [Header ( "Abilities" )]
        public List<GenericSpell> abilities = new List<GenericSpell> ();

        [Header ( "Canvas Barre d'Action" )]
        public GameObject abilitiesButtons;

        [HideInInspector]
        public int currentActionPoints;
        [SerializeField]
        protected GenericAbility activeAbility;
        [HideInInspector]
        public List<GenericEffect> effects = new List<GenericEffect> ();

        protected List<GenericEffect> onTakeDamagesEffects = new List<GenericEffect> ();
        protected List<GenericEffect> onDealDamagesEffects = new List<GenericEffect> ();
        protected List<GenericEffect> onKillEffects = new List<GenericEffect> ();
        protected List<GenericEffect> onStartOfTurnEffects = new List<GenericEffect> ();
        protected List<GenericEffect> onsEndOfTurnEffects = new List<GenericEffect> ();
        protected List<GenericEffect> onSpawnEffects = new List<GenericEffect> ();
        protected List<GenericEffect> onDeathEffects = new List<GenericEffect> ();
        protected List<GenericEffect> onMoveEffects = new List<GenericEffect> ();
        protected List<GenericEffect> onDashEffects = new List<GenericEffect> ();

        public IEnumerator initCharacter ()
        {
            // ----- Initialisation de Personnage ----- //

            // ----- Initialisation Valeurs ----- //
            currentActionPoints = actionPoints;

            // ----- Initialisation Canvas Flottant ----- //
            characterInfos.setUp ( this );

            // ----- Initialisation des Actions ----- //
            for ( int i = 0 ; i < abilities.Count ; i++ )
            {
                int j = i;
                abilities[ i ] = Instantiate ( abilities[ i ] , abilitiesButtons.transform );
                GameObject obj = Instantiate ( SpellComponentsManager.getInstance ().abilityButton , abilities[ i ].transform );
                obj.GetComponent<Image> ().sprite = abilities[ i ].spellSprite;
                obj.GetComponent<Button> ().onClick.AddListener ( delegate { clicAbilityButton ( j ); } );
                abilities[ i ].GetComponent<AbilityComponents> ().cooldownText = obj.transform.Find ( "Cooldown" ).GetComponent<Text> ();
                abilities[ i ].GetComponent<AbilityComponents> ().chargesCooldownText = obj.transform.Find ( "Charge Cooldown" ).GetComponent<Text> ();
                abilities[ i ].GetComponent<AbilityComponents> ().chargesText = obj.transform.Find ( "Charges" ).GetComponent<Text> ();
                abilities[ i ].setAbility ( this );
                obj.GetComponent<SpellToolTip> ().setUp ( abilities[ i ] );
            }
            LayoutRebuilder.ForceRebuildLayoutImmediate ( abilitiesButtons.GetComponent<RectTransform> () );

            // ----- Finalisation ----- //
            characterCircle.transform.localScale = Vector3.one * characterRadius * 2;
            GetComponent<CapsuleCollider> ().radius = characterRadius;
            characterInfos.updateInfos ();

            yield return null;
        }

        /// <summary>
        /// Active la capacité à l'index dans la liste
        /// </summary>
        /// <param name="index"></param>
        public void clicAbilityButton ( int index )
        {
            if ( activeAbility )
                activeAbility.setInactive ();
            activeAbility = abilities[ index ];
            activeAbility.setActive ();
        }

        public void addEffect ( GenericEffect effect )
        {
            effects.Add ( effect );
        }

        public void addOnTakeDamagesEffects ( GenericEffect effect )
        {
            onTakeDamagesEffects.Add ( effect );
        }

        public void addOnDealDamagesEffects ( GenericEffect effect )
        {
            onDealDamagesEffects.Add ( effect );
        }

        public void addOnKillEffects ( GenericEffect effect )
        {
            onKillEffects.Add ( effect );
        }

        public void addOnStartOfTurnEffects ( GenericEffect effect )
        {
            onStartOfTurnEffects.Add ( effect );
        }

        public void addOnsEndOfTurnEffects ( GenericEffect effect )
        {
            onsEndOfTurnEffects.Add ( effect );
        }

        public void addOnSpawnEffects ( GenericEffect effect )
        {
            onSpawnEffects.Add ( effect );
        }

        public void addOnDeathEffects ( GenericEffect effect )
        {
            onDeathEffects.Add ( effect );
        }

        public void addOnMoveEffects ( GenericEffect effect )
        {
            onMoveEffects.Add ( effect );
        }

        public void addOnDashEffects ( GenericEffect effect )
        {
            onDashEffects.Add ( effect );
        }










        //TODO:revoir la suite

        public int mitigateHeal ( Damage heal )
        {
            return Mathf.Max ( 0 , heal.amount );
        }

        public int mitigateReceivedDamages ( Damage dmg )
        {
            int mitigatedDamages = 0;

            for ( int i = 0 ; i < resistances.Length ; i++ )
                if ( resistances[ i ].type == dmg.type )
                    mitigatedDamages -= resistances[ i ].amount;

            return mitigatedDamages;
        }

        public int mitigateDealingDamages ( Damage dmg )
        {
            int mitigatedDamages = 0;

            for ( int i = 0 ; i < effects.Count ; i++ )
                mitigatedDamages += effects[ i ].mitigateDamages ( dmg.amount );

            return mitigatedDamages;
        }

        public void startNewTurn ()
        {
            currentActionPoints = actionPoints;
            characterInfos.updateInfos ();
            foreach ( GenericAbility ability in abilities )
                ability.newTurnAbility ();
            activeAbility.setActive ();

            activateAbilityButtons ();
        }

        public void setActiveAbility ( GenericAbility ability )
        {
            if ( activeAbility )
                activeAbility.setInactive ();
            activeAbility = ability;
            activeAbility.setActive ();
        }

        public void resetAbility ()
        {
            activeAbility = null;
        }

        public void activateAbilityButtons ()
        {
            foreach ( GenericAbility ability in abilities )
                ability.activateButton ();
        }

        internal void decreaseActionPoint ( int nbPoint )
        {
            currentActionPoints -= nbPoint;
        }

        public void addDamagePreview ( int amount )
        {
            characterInfos.addDamagePreview ( amount );
        }

        public int CompareTo ( object obj )
        {
            if ( obj == null )
                return 1;

            return ( name.CompareTo ( ( obj as Character ).name ) );
        }

        public void disableAbilityButtons ()
        {
            foreach ( GenericAbility abil in abilities )
                abil.desactivateButton ();
        }

        public void takeDamages ( Character source , Damage dmg )
        {

        }
    }
}