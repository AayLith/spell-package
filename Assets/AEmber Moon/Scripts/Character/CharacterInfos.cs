﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Æmber_Moon
{
    public class CharacterInfos : MonoBehaviour
    {
        protected Character perso;

        [SerializeField]
        protected Text textName;
        [SerializeField]
        public float vertivalOffset = 0;
        [SerializeField]
        private Text damagePreviewText;
        private int damagesToPreview;

        /// <summary>
        /// Initialise les sliders et les textes
        /// </summary>
        /// <param name="perso"></param>
        public virtual void setUp ( Character perso )
        {
            this.perso = perso;
            textName.text = perso.characterName;
        }

        public virtual void updateInfos ()
        {
            resetDamagePreview ();
            posInfos ();
        }

        public void posInfos ()
        {
            if ( !perso )
                return;
            transform.position = Camera.main.WorldToScreenPoint ( perso.transform.position );
            transform.position = new Vector3 ( transform.position.x , transform.position.y + vertivalOffset , 0 );
        }

        public void resetDamagePreview ()
        {
            damagesToPreview = 0;
            damagePreviewText.text = "";
        }

        public void addDamagePreview ( int amount )
        {
            damagesToPreview += amount;
            damagePreviewText.text = Mathf.Max ( damagesToPreview , -damagesToPreview ).ToString ();
            if ( damagesToPreview > 0 )
                damagePreviewText.GetComponent<Outline> ().effectColor = Color.red;
            else if ( damagesToPreview < 0 )
                damagePreviewText.GetComponent<Outline> ().effectColor = new Color32 ( 0 , 200 , 0 , 128 );
            else damagePreviewText.text = "";
        }
    }
}