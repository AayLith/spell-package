﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Æmber_Moon
{
    public class TakeDamageAnimation : MonoBehaviour
    {
        private bool firstDisplayDone = false;
        private int damageToDisplay = 0;
        public Text secondaryText;
        public Text mainText;

        [Header ( "Animation" )]
        public int animationSteps = 10;
        public int secondAnimationSteps = 5;
        public float startingPos = -250;
        public float endPos = -75;
        public float startingScale = 0.1f;
        public float endScale = 1.5f;
        public float biggerScale = 2f;

        private bool isAnimatingFirstStep = false;

        private void Start ()
        {
            mainText = Instantiate ( mainText , transform );
            mainText.transform.position = Vector3.zero;
            mainText.gameObject.SetActive ( false );
        }

        public void resetDamages ()
        {
            mainText.text = "";
            mainText.gameObject.SetActive ( false );
            firstDisplayDone = false;
            damageToDisplay = 0;
        }

        public void addDamageTaken ( int amount )
        {
            damageToDisplay += amount;

            if ( !firstDisplayDone )
            {
                if ( amount > 0 )
                    mainText.GetComponent<Outline> ().effectColor = Color.red;
                else if ( amount < 0 )
                    mainText.GetComponent<Outline> ().effectColor = new Color32 ( 0 , 200 , 0 , 128 );
                else
                    mainText.GetComponent<Outline> ().effectColor = Color.black;

                StartCoroutine ( animateMainText () );
            }

            else
            {
                StartCoroutine ( animateSecondaryText ( amount ) );
            }
        }

        private IEnumerator animateMainText ()
        {
            isAnimatingFirstStep = true;

            float posStep = ( endPos - startingPos ) / animationSteps;
            float scaleStep = ( endScale - startingScale ) / animationSteps;

            mainText.gameObject.SetActive ( true );
            mainText.rectTransform.localPosition = new Vector3 ( 0 , startingPos , 0 );
            mainText.rectTransform.localScale = Vector3.one * startingScale;
            mainText.text = Mathf.Max ( damageToDisplay , -damageToDisplay ).ToString ();

            int i = 0;
            while ( i < animationSteps )
            {
                mainText.rectTransform.localPosition = new Vector3 ( 0 , mainText.rectTransform.localPosition.y + posStep , 0 );
                mainText.rectTransform.localScale = mainText.rectTransform.localScale + Vector3.one * scaleStep;
                i++;
                yield return null;
            }
            firstDisplayDone = true;
            isAnimatingFirstStep = false;

            yield return null;
        }

        private IEnumerator animateSecondaryText ( int amount )
        {
            while ( isAnimatingFirstStep )
                yield return null;

            float posStep = ( endPos - startingPos ) / animationSteps;
            float scaleStep = ( biggerScale - endScale ) / secondAnimationSteps;

            GameObject newText = Instantiate ( secondaryText.gameObject , transform );

            newText.transform.SetAsFirstSibling ();
            newText.GetComponent<Text> ().text = Mathf.Max ( amount , -amount ).ToString ();
            if ( amount > 0 )
                newText.GetComponent<Outline> ().effectColor = Color.red;
            else if ( amount < 0 )
                newText.GetComponent<Outline> ().effectColor = new Color32 ( 0 , 200 , 0 , 128 );
            else
                newText.GetComponent<Outline> ().effectColor = Color.black;

            newText.GetComponent<RectTransform> ().localPosition = new Vector3 ( 0 , startingPos , 0 );
            newText.GetComponent<RectTransform> ().localScale = Vector3.one;

            int i = 0;
            while ( i < animationSteps )
            {
                newText.GetComponent<RectTransform> ().localPosition = new Vector3 ( 0 , newText.GetComponent<RectTransform> ().localPosition.y + posStep , 0 );
                i++;
                yield return null;
            }

            Destroy ( newText );

            mainText.text = Mathf.Max ( damageToDisplay , -damageToDisplay ).ToString ();
            if ( amount > 0 )
                mainText.GetComponent<Outline> ().effectColor = Color.red;
            else if ( amount < 0 )
                mainText.GetComponent<Outline> ().effectColor = new Color32 ( 0 , 200 , 0 , 128 );
            else
                mainText.GetComponent<Outline> ().effectColor = Color.black;

            i = 0;
            while ( i < secondAnimationSteps )
            {
                mainText.rectTransform.localScale = mainText.rectTransform.localScale + Vector3.one * scaleStep;
                i++;
                yield return null;
            }

            i = 0;
            while ( i < secondAnimationSteps )
            {
                mainText.rectTransform.localScale = mainText.rectTransform.localScale - Vector3.one * scaleStep;
                i++;
                yield return null;
            }

            yield return null;
        }
    }
}