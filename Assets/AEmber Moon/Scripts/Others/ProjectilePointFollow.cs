﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class ProjectilePointFollow : MonoBehaviour
    {
        public GameObject impact;
        protected List<ParticleSystem> particleSys = new List<ParticleSystem> ();
        public float timeBeforeDestroy = 1;

        protected float speed;
        protected IndicatorData datas;

        protected int index = 1;
        protected bool impactSpawned = false;

        public void setUpProjectile ( float speed , float spawnHeight , IndicatorData datas )
        {
            this.speed = speed;
            this.datas = datas;
            transform.rotation = datas.rotation;
            transform.position = datas.positions[ 0 ] + new Vector3 ( 0 , spawnHeight , 0 );
            particleSys.AddRange ( GetComponentsInChildren<ParticleSystem> () );
        }

        private void FixedUpdate ()
        {
            if ( index < datas.positions.Count )
            {
                transform.LookAt ( datas.positions[ index ] );
                transform.position = Vector3.MoveTowards ( transform.position , datas.positions[ index ] , speed * Time.deltaTime );
                if ( transform.position == datas.positions[ index ] )
                    index += 1;
            }
            else if ( !impactSpawned )
            {
                if ( impact )
                {
                    GameObject imp = Instantiate ( impact );
                    imp.transform.position = transform.position;
                    imp.transform.rotation = transform.rotation;
                }
                impactSpawned = true;

                if ( particleSys.Count > 0 )
                {
                    foreach ( ParticleSystem psys in particleSys )
                    {
                        var em = psys.emission;
                        em.enabled = false;
                    }
                }

                Destroy ( gameObject , timeBeforeDestroy );
            }
        }
    }
}