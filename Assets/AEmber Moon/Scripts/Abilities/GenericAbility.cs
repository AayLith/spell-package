﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Æmber_Moon
{
    public abstract class GenericAbility : MonoBehaviour
    {
        [Header ( "Generic Abilty" )]
        [SerializeField]
        public string abilityName;
        public int APCost = 1;
        public string description = "No description.";
        [HideInInspector]
        public List<MonoGenericEffect> effects = new List<MonoGenericEffect> ();

        //[HideInInspector]
        public Character lanceur;

        public virtual void OnValidate ()
        {
            APCost = Mathf.Max ( APCost , 0 );
        }

        public virtual void setAbility ( Character lanceur )
        {
            this.lanceur = lanceur;
        }

        public virtual void validateSpell ( List<IndicatorTarget> targets )
        {
            lanceur.decreaseActionPoint ( APCost );
            StartCoroutine ( useAbility ( targets ) );
            setInactive ();
        }

        /// <summary>
        /// Effectue l'action
        /// </summary>
        protected abstract IEnumerator useAbility ( List<IndicatorTarget> targets );

        /// <summary>
        /// Affiche les indicateurs et passe 'isActive' à true pour permettre à la capacité de s'actualiser
        /// </summary>
        public abstract void setActive ();

        /// <summary>
        /// Cache l'indicateur principal et détruit les autres. Passe 'isActive' à false pour empêcher la capacité de s'actualiser
        /// </summary>
        public abstract void setInactive ();

        public virtual void newTurnAbility ()
        {

        }

        public virtual void activateButton ()
        {
            if ( lanceur.currentActionPoints >= APCost )
                GetComponentInChildren<Button> ().interactable = true;
            else
                GetComponentInChildren<Button> ().interactable = false;
        }

        public virtual void desactivateButton ()
        {
            GetComponentInChildren<Button> ().interactable = false;
        }
    }
}