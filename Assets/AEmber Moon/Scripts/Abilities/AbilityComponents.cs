﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Æmber_Moon
{
    public class AbilityComponents : MonoBehaviour
    {
        public Text cooldownText;
        public Text chargesCooldownText;
        public Text chargesText;
    }
}