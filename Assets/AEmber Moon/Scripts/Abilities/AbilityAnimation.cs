﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class AbilityAnimation : MonoBehaviour
    {
        public GameObject projectile;
        public float projectileSpeed;
        public float spawnHeight;
        public float delayBetweenProjectiles = 0.15f;
        public float projectileLifeTime = 1;
        public float animationLength = 2;

        public IEnumerator createProjectiles ()
        {
            List<IndicatorData> indicatorDatas = GetComponent<IndicatorStrategy> ().getPositionsAndRotationsFromIndicators ();

            foreach (IndicatorData data in indicatorDatas)
            {
                var proj = Instantiate ( projectile );
                proj.GetComponent<ProjectilePointFollow> ().setUpProjectile ( projectileSpeed , spawnHeight , data );
            }

            yield return null;
        }
    }
}