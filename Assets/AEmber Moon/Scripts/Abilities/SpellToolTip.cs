﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Æmber_Moon
{
    public class SpellToolTip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public int verticalOffset = 55;
        public GameObject toolTip;
        public Text texteNom;
        public Text texteApcost;
        public Image line;
        public Text texteDescription;
        public Image image;
        public GenericAbility ability;

        public void setUp ( GenericSpell ability )
        {
            this.ability = ability;

            texteNom.text = ability.abilityName;
            texteApcost.text = ability.APCost.ToString ();

            string descr = ability.description;
            foreach ( MonoGenericEffect effect in ability.effects )
            {
                var text = effect.getDescription ();
                if ( text.Length > 0 )
                    descr += "\n" + text;
            }
            descr = descr.TrimEnd ( '\n' );
            texteDescription.text = descr;

            image.rectTransform.position = new Vector2 ( image.rectTransform.position.x , verticalOffset );

            toolTip.SetActive ( false );
        }

        protected void actualise ()
        {
            texteNom.text = ability.abilityName;
            texteApcost.text = ability.APCost.ToString ();

            string descr = ability.description + "\n\n";
            foreach ( MonoGenericEffect effect in ability.effects )
            {
                var text = effect.getDescription ();
                if ( text.Length > 0 )
                    descr += text + " \n";
            }
            descr = descr.TrimEnd ( '\n' );
            texteDescription.text = descr;

            image.rectTransform.position = new Vector2 ( image.rectTransform.position.x , verticalOffset );
        }

        public void OnPointerEnter ( PointerEventData eventData )
        {
            toolTip.SetActive ( true );
            actualise ();
        }

        public void OnPointerExit ( PointerEventData eventData )
        {
            toolTip.SetActive ( false );
        }
    }
}