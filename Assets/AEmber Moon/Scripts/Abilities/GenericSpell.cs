﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Æmber_Moon
{
    /// <summary>
    /// 
    /// </summary>
    public class GenericSpell : GenericAbility
    {
        public Sprite spellSprite;

        [Header ( " Spell Cooldown" )]
        public int cooldown = 1;
        protected int currentCooldown;
        public int charges = 1;
        protected int currentCharges;

        ////////////////////////////////////////////////////
        // Private elements
        [HideInInspector]
        public IndicatorStrategy targetingIndicator;

        protected Vector3 worldPos;
        protected bool isActive = false;
        protected Dictionary<Character , int> targetingIndicatorTargets = new Dictionary<Character , int> ();
        protected List<Dictionary<Character , int>> previewedDamages = new List<Dictionary<Character , int>> ();

        /// <summary>
        /// Vérifie qu'ile n'y a pas de valeurs abérantes dans l'éditeur
        /// </summary>
        public override void OnValidate ()
        {
            base.OnValidate ();
            cooldown = Mathf.Max ( cooldown , 1 );
            charges = Mathf.Max ( charges , 1 );
        }

        // ----- Instantie les composants du sort ----- //
        public override void setAbility ( Character lanceur )
        {
            base.setAbility ( lanceur );

            // ----- Initialisation de Sort ----- //
            targetingIndicator = GetComponent<IndicatorStrategy> ();
            foreach ( MonoGenericEffect effect in GetComponents<MonoGenericEffect> () )
            {
                effects.Add ( effect );
                effect.source = lanceur;
            }

            targetingIndicator.init ();
            targetingIndicator.spell = this;

            currentCharges = charges;
            currentCooldown = cooldown;

            activateButton ();
            cooldownTextsManager ( GetComponent<AbilityComponents> ().chargesText , GetComponent<AbilityComponents> ().cooldownText , GetComponent<AbilityComponents> ().chargesCooldownText );
        }

        /// <summary>
        /// Gère le cooldown du sort
        /// </summary>
        protected void cooldownManager ()
        {
            if ( currentCharges < charges )                                 // Seulement si le sort n'est pas complètement chargé
            {
                currentCooldown--;
                if ( currentCooldown <= 0 )
                {
                    currentCharges++;
                    currentCooldown = cooldown;
                }
                cooldownTextsManager ( GetComponent<AbilityComponents> ().chargesText , GetComponent<AbilityComponents> ().cooldownText , GetComponent<AbilityComponents> ().chargesCooldownText );                                    // on actualise les textes
            }
        }

        /// <summary>
        /// Gère l'affichage du cooldown et des charges du sort
        /// </summary>
        public void cooldownTextsManager ( Text chargesText , Text cooldownText , Text chargesCooldownText )
        {
            if ( currentCharges >= charges )                                // chargesText seuleument si currentCharges >= 1
            {
                if ( charges == 1 )
                    chargesText.text = "";
                else
                    chargesText.text = currentCharges.ToString ();
                cooldownText.text = "";
                chargesCooldownText.text = "";
            }
            else if ( charges == 1 )                                        // cooldownText seuleument si currentCooldown >= 1
            {
                if ( currentCharges < charges )
                    cooldownText.text = currentCooldown.ToString ();
                else
                    cooldownText.text = "";
                chargesCooldownText.text = "";
                chargesText.text = "";
            }
            else if ( charges > 1 )                                         // Si le sort peut avoir plus d'une seule charge
            {
                if ( currentCharges == 0 )                                  // cooldownText seuleument si currentCooldown >= 1
                {
                    cooldownText.text = currentCooldown.ToString ();
                    chargesCooldownText.text = "";
                    chargesText.text = "";
                }
                else                                                        // chargesCooldownText seuleument si currentCooldown >= 1 et chargesText seuleument si currentCharges >= 1
                {
                    cooldownText.text = "";
                    chargesCooldownText.text = currentCooldown.ToString ();
                    chargesText.text = currentCharges.ToString ();
                }
            }
        }

        /// <summary>
        /// Active et désactive le bouton s'il y a assez ou pas de charges et de PA. A appeller chaque fois que l'un ou l'autre est modifié
        /// </summary>
        public override void activateButton ()
        {
            if ( currentCharges >= 1 && lanceur.currentActionPoints >= APCost )
                GetComponentInChildren<Button> ().interactable = true;
            else
                GetComponentInChildren<Button> ().interactable = false;
        }

        /// <summary>
        /// Les actions que le sort doit faire au début d'un nouveau tour
        /// </summary>
        public override void newTurnAbility ()
        {
            cooldownManager ();
            activateButton ();
        }

        /// <summary>
        /// Rend le sort actif et lui permet de s'actualiser
        /// </summary>
        public override void setActive ()
        {
            IndicatorStrategyManager.getInstance ().setIndicator ( targetingIndicator , this );

            isActive = true;
        }

        /// <summary>
        /// Rend le sort inactif et détruit tout ce qui doit l'être
        /// </summary>
        public override void setInactive ()
        {
            foreach ( MonoGenericEffect effect in effects )
                effect.undoPreviewEffect ();

            IndicatorStrategyManager.getInstance ().unsetIndicator ();

            lanceur.resetAbility ();

            isActive = false;
        }

        public override void validateSpell ( List<IndicatorTarget> targets )
        {
            currentCharges--;
            lanceur.decreaseActionPoint ( APCost );
            cooldownTextsManager ( GetComponent<AbilityComponents> ().chargesText , GetComponent<AbilityComponents> ().cooldownText , GetComponent<AbilityComponents> ().chargesCooldownText );
            activateButton ();
            StartCoroutine ( useAbility ( targets ) );
            setInactive ();
        }

        protected override IEnumerator useAbility ( List<IndicatorTarget> targets )
        {
            StartCoroutine ( GetComponent<AbilityAnimation> ().createProjectiles () );
            foreach ( var target in targets )
                foreach ( MonoGenericEffect effect in effects )
                    effect.applyEffect ( target.target.GetComponent<Character> () );
            yield return null;
        }
    }
}