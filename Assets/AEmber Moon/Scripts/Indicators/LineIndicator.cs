﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class LineIndicator : Indicator
    {
        public float tailLength = 0.1f;
        public float headLength = 0.1f;
        public GameObject head;
        public GameObject body;
        public GameObject tail;
        public new GameObject collider;
        [HideInInspector]
        public float length;
        public Indicator tipIndicator;

        public void setUp ( float range , float width )
        {
            tail.transform.localScale = new Vector3 ( width , 1 , width );
            head.transform.localScale = new Vector3 ( width , width , 1 );
            body.transform.localScale = new Vector3 ( width , ( range - ( tailLength + headLength ) * width ) , 1 );
            collider.transform.localScale = new Vector3 ( width , 1 , range );
            head.transform.localPosition = new Vector3 ( 0 , 0.05f , range );
            body.transform.localPosition = new Vector3 ( 0 , 0.05f , range / 2 );
            collider.transform.localPosition = new Vector3 ( 0 , 0.05f , range / 2 );
            length = range;

            if ( tipIndicator )
                tipIndicator.transform.position = head.transform.position;
        }

        public void setUpWithoutCollider ( float range , float width )
        {
            tail.transform.localScale = new Vector3 ( width , 1 , width );
            head.transform.localScale = new Vector3 ( width , width , 1 );
            body.transform.localScale = new Vector3 ( width , ( range - ( tailLength + headLength ) * width ) , 1 );
            head.transform.localPosition = new Vector3 ( 0 , 0.05f , range );
            body.transform.localPosition = new Vector3 ( 0 , 0.05f , range / 2 );
            length = range;

            if ( tipIndicator )
                tipIndicator.transform.position = head.transform.position;
        }
    }
}