﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class ColliderInfos : MonoBehaviour
    {
        public List<GameObject> colliders = new List<GameObject> ();

        public List<GameObject> getCollisions ()
        {
            return colliders;
        }

        private void OnTriggerEnter ( Collider other )
        {
            if ( !colliders.Contains ( other.gameObject ) )
            {
                colliders.Add ( other.gameObject );
            }
        }

        private void OnTriggerExit ( Collider other )
        {
            if ( colliders.Contains ( other.gameObject ) )
            {
                colliders.Remove ( other.gameObject );
            }
        }

        public void resetColliders ()
        {
            colliders = new List<GameObject> ();
        }
    }
}