﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    /// <summary>
    /// Gère les GameObjects utilisés comme indicateurs
    /// </summary>
    public class Indicator : MonoBehaviour
    {
        public Indicator ()
        {

        }

        private void Start ()
        {
            SetActive ( false );
        }

        /// <summary>
        /// Activate/Desactivate the GameObject
        /// </summary>
        /// <param name="state"></param>
        public virtual void SetActive ( bool state )
        {
            gameObject.SetActive ( state );
        }
    }
}