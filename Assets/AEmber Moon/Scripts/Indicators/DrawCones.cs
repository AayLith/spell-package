﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class DrawCones : MonoBehaviour
    {
        public int quality = 15;
        Mesh[] meshs;
        public Material material;
        public GameObject leftArm;
        public GameObject rightArm;

        public float angle_fov;
        public float dist_max;
        public float dist_min;
        public float line_width = 0.1f;

        public float[] distances;

        public void setUp ( int count )
        {
            if ( count > 0 )
            {
                if ( meshs != null )
                {
                    for ( int i = 0 ; i < meshs.Length ; i++ )
                        Destroy ( meshs[ i ] );
                }

                meshs = new Mesh[ count + 1 ];
                for ( int i = 0 ; i < count + 1 ; i++ )
                {
                    meshs[ i ] = new Mesh ();
                }
            }
        }

        public void drawConcentricCones ( float angle , float[] radiuses , float minRadius = 0.5f )
        {
            angle_fov = angle / 2;
            distances = radiuses;
            dist_min = minRadius;
            dist_max = radiuses[ radiuses.Length - 1 ];

            drawCone ( angle / 2 , minRadius - line_width , minRadius + line_width , meshs[ meshs.Length - 1 ] );
            for ( int i = 0 ; i < radiuses.Length ; i++ )
                drawCone ( angle / 2 , radiuses[ i ] - line_width , radiuses[ i ] + line_width , meshs[ i ] );

            leftArm.transform.localRotation = Quaternion.Euler ( new Vector3 ( 0 , -angle / 2 +180, 0 ) );
            rightArm.transform.localRotation = Quaternion.Euler ( new Vector3 ( 0 , angle / 2 +180, 0 ) );
            leftArm.transform.localScale = new Vector3 ( 10 , 10 , radiuses[ 0 ] * 2 );
            rightArm.transform.localScale = new Vector3 ( 10 , 10 , radiuses[ 0 ] * 2 );
        }

        protected void drawCone ( float angle , float inRadius , float outRadius , Mesh mesh )
        {
            dist_min = inRadius;
            dist_max = outRadius;

            mesh.vertices = new Vector3[ 4 * quality ];   // Could be of size [2 * quality + 2] if circle segment is continuous
            mesh.triangles = new int[ 3 * 2 * quality ];

            Vector3[] normals = new Vector3[ 4 * quality ];
            Vector2[] uv = new Vector2[ 4 * quality ];

            for ( int i = 0 ; i < uv.Length ; i++ )
                uv[ i ] = new Vector2 ( 0 , 0 );
            for ( int i = 0 ; i < normals.Length ; i++ )
                normals[ i ] = new Vector3 ( 0 , 1 , 0 );

            mesh.uv = uv;
            mesh.normals = normals;

            float angle_lookat = GetEnemyAngle ();

            float angle_start = angle_lookat - angle_fov;
            float angle_end = angle_lookat + angle_fov;
            float angle_delta = ( angle_end - angle_start ) / quality;

            float angle_curr = angle_start;
            float angle_next = angle_start + angle_delta;

            Vector3 pos_curr_min;
            Vector3 pos_curr_max;

            Vector3 pos_next_min;
            Vector3 pos_next_max;

            Vector3[] vertices = new Vector3[ 4 * quality ];   // Could be of size [2 * quality + 2] if circle segment is continuous
            int[] triangles = new int[ 3 * 2 * quality ];

            for ( int i = 0 ; i < quality ; i++ )
            {
                Vector3 sphere_curr = new Vector3 (
                Mathf.Sin ( Mathf.Deg2Rad * ( angle_curr ) ) , 0 ,   // Left handed CW
                Mathf.Cos ( Mathf.Deg2Rad * ( angle_curr ) ) );

                Vector3 sphere_next = new Vector3 (
                Mathf.Sin ( Mathf.Deg2Rad * ( angle_next ) ) , 0 ,
                Mathf.Cos ( Mathf.Deg2Rad * ( angle_next ) ) );

                pos_curr_min = transform.position + sphere_curr * dist_min;
                pos_curr_max = transform.position + sphere_curr * dist_max;

                pos_next_min = transform.position + sphere_next * dist_min;
                pos_next_max = transform.position + sphere_next * dist_max;

                int a = 4 * i;
                int b = 4 * i + 1;
                int c = 4 * i + 2;
                int d = 4 * i + 3;

                vertices[ a ] = pos_curr_min;
                vertices[ b ] = pos_curr_max;
                vertices[ c ] = pos_next_max;
                vertices[ d ] = pos_next_min;

                triangles[ 6 * i ] = a;       // Triangle1: abc
                triangles[ 6 * i + 1 ] = b;
                triangles[ 6 * i + 2 ] = c;
                triangles[ 6 * i + 3 ] = c;   // Triangle2: cda
                triangles[ 6 * i + 4 ] = d;
                triangles[ 6 * i + 5 ] = a;

                angle_curr += angle_delta;
                angle_next += angle_delta;

            }

            mesh.vertices = vertices;
            mesh.triangles = triangles;

            Graphics.DrawMesh ( mesh , Vector3.zero , Quaternion.identity , material , 0 );
        }

        float GetEnemyAngle ()
        {
            return 90 - Mathf.Rad2Deg * Mathf.Atan2 ( transform.forward.z , transform.forward.x ); // Left handed CW. z = angle 0, x = angle 90
        }
    }
}