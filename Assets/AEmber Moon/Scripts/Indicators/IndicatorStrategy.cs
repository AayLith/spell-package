﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Æmber_Moon
{
    public abstract class IndicatorStrategy : MonoBehaviour
    {
        [Range ( 1 , 10 )]
        public int numberOfTargetings = 1;
        [HideInInspector]
        public GenericAbility spell;
        protected Indicator indicator;
        protected List<Indicator> validIndicators = new List<Indicator> ();
        protected List<Indicator> finalIndicators = new List<Indicator> ();
        [HideInInspector]
        public bool isHidden;

        [Header ( "Targets" )]
        public bool canHitUser = false;
        public bool canHitTeam = false;
        public bool canHitEnemies = true;
        public bool canHitMultipleTimes = true;
        public bool canTargetOverWalls = false;
        public bool canHitThroughWalls = false;

#if UNITY_EDITOR
        public virtual void OnValidate ()
        {
            editorWarning ();
        }
#endif

        /// <summary>
        /// Hides the indicators if the pointer is over the UI, otherwise, unhide them and call the functions to update them.
        /// </summary>
        public void update ()
        {
            if ( EventSystem.current.IsPointerOverGameObject () ) // && !isHidden ) 
            {
                startHide ();
            }
            else
            {
                // if ( isHidden )
                startUnhide ();
                validUpdate ();
                targetingPreview ();
            }
        }

        /// <summary>
        /// Called by update(). Updates the positions and rotation of the indicators
        /// </summary>
        protected abstract void validUpdate ();

        /// <summary>
        /// Initialise the variables before firts-time use.
        /// </summary>
        public abstract void init ();

        /// <summary>
        /// Initialise the indicators before use.
        /// </summary>
        public abstract void setUp ();

        /// <summary>
        /// Hides the indicators.
        /// </summary>
        protected abstract void hide ();

        /// <summary>
        /// Unhides the indicators.
        /// </summary>
        protected abstract void unHide ();

        /// <summary>
        /// Reset the variables after use.
        /// </summary>
        public abstract void reset ();

        /// <summary>
        /// Return a list of IndicatorTargets (GameObject, Distance, Index) containing all the valid targets for this strategy.
        /// </summary>
        /// <returns></returns>
        protected abstract List<IndicatorTarget> getAllTargets ();

        /// <summary>
        /// Returns a list of position for each indicator in the strategy and the rotation for the indicator.
        /// </summary>
        /// <returns></returns>
        public abstract List<IndicatorData> getPositionsAndRotationsFromIndicators ();

#if UNITY_EDITOR
        /// <summary>
        /// Display a warning in the console if the effects parameters do not match this strategy. 
        /// </summary>
        protected abstract void editorWarning ();
#endif

        /// <summary>
        /// Perform the action when left-clicking. Usually validing the indicator and the spell.
        /// </summary>
        public virtual void clicGauche ()
        {
            if ( numberOfTargetings == 1 )
            {
                oneTargeting ();
            }
            else
            {
                multipleTargetings ();
            }
        }

        /// <summary>
        /// Perform the action when right-clicking. Usually go back one step or returning to default ability.
        /// </summary>
        public virtual void clicDroit ()
        {
            if ( validIndicators.Count > 0 )
                goBack ();
            else
                spell.setInactive ();
        }

        /// <summary>
        /// If the strategy need only one targeting, it add the indicator to the finalIndicators then validate the spell.
        /// </summary>
        public virtual void oneTargeting ()
        {
            finalIndicators.Add ( Instantiate ( indicator ) );
            spell.validateSpell ( getAllTargets () );
        }

        /// <summary>
        /// If the strategy need multiple targetings, it add the indicator to the validIndicators and if it has enough, it add them to finalIndicators then validate the spell.
        /// </summary>
        public virtual void multipleTargetings ()
        {
            validIndicators.Add ( Instantiate ( indicator ) );
            if ( validIndicators.Count == numberOfTargetings )
            {
                finalIndicators.AddRange ( validIndicators );
                validIndicators = new List<Indicator> ();
                spell.validateSpell ( getAllTargets () );
            }
        }

        /// <summary>
        /// Remove the last item from validIndicators.
        /// </summary>
        public virtual void goBack ()
        {
            Destroy ( validIndicators[ validIndicators.Count - 1 ] );
            validIndicators.RemoveAt ( validIndicators.Count - 1 );
        }

        /// <summary>
        /// Call the preview function on the spell effects, sending them the targets currently in the indicators.
        /// </summary>
        public virtual void targetingPreview ()
        {
            List<IndicatorTarget> targets = getAllTargets ();

            foreach ( MonoGenericEffect effect in spell.effects )
                effect.previewEffect ( targets );
        }

        /// <summary>
        /// Returns the targets in an indicator with their distance from it.
        /// </summary>
        /// <param name="indicator"> The indicator in which the targets are retrieved </param>
        /// <returns></returns>
        protected virtual List<KeyValuePair<GameObject , float>> getTargets ( Indicator indicator )
        {
            List<KeyValuePair<GameObject , float>> targets = new List<KeyValuePair<GameObject , float>> ();
            foreach ( GameObject target in indicator.GetComponentInChildren<ColliderInfos> ().getCollisions () )
            {
                if ( target != null && isValidTarget ( target, indicator.transform.position ) )
                {
                    KeyValuePair<GameObject , float> kvp = new KeyValuePair<GameObject , float> ( target , Vector3.Distance ( indicator.transform.position , target.transform.position ) - target.GetComponent<Character> ().characterRadius ); ;
                    if ( canHitMultipleTimes || !targets.Contains ( kvp ) )
                        targets.Add ( kvp );
                }
            }
            return targets;
        }

        /// <summary>
        /// Return the target with the closest distance.
        /// </summary>
        /// <param name="targets"> List of <KeyValuePair<GameObject , float> where the smallest vallue is extracted </param>
        /// <returns></returns>
        protected virtual KeyValuePair<GameObject , float> getClosestTarget ( List<KeyValuePair<GameObject , float>> targets )
        {
            if ( targets.Count > 0 )
            {
                targets.Sort ( ( x , y ) => x.Value.CompareTo ( y.Value ) );
                return new KeyValuePair<GameObject , float> ( targets[ 0 ].Key , targets[ 0 ].Value );
            }
            else
                return new KeyValuePair<GameObject , float> ();
        }

        /// <summary>
        /// Return true if the position is inside the cone.
        /// </summary>
        /// <param name="pos"> The position to check </param>
        /// <param name="indicator"> the cone Indicator. Must have a Cone component </param>
        /// <returns></returns>
        protected bool verifIfPosIsInCone ( Vector3 pos , Indicator indicator )
        {
            return ( Vector3.Angle ( pos - indicator.transform.position , indicator.transform.up ) < indicator.GetComponentInChildren<DrawCones> ().angle_fov / 2 );
        }

        /// <summary>
        /// Return true if the target is inside the cone.
        /// </summary>
        /// <param name="target"> The object to check </param>
        /// <param name="indicator"> the cone Indicator. Must have a Cone component </param>
        /// <returns></returns>
        protected bool verifIfTargetIsInCone ( GameObject target , Indicator indicator )
        {
            if ( verifIfPosIsInCone ( target.transform.position , indicator ) )
            {
                return true;
            }
            else
            {
                Vector3 direction = target.transform.position - indicator.transform.position;
                Vector3 cross = new Vector3 ( direction.z , target.transform.position.y , -direction.x ).normalized * target.GetComponent<Character> ().characterRadius;
                Vector3 newSpot = target.transform.position + cross;

                if ( verifIfPosIsInCone ( newSpot , indicator ) )
                    return true;

                newSpot = target.transform.position - cross;

                if ( verifIfPosIsInCone ( newSpot , indicator ) )
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Place the indicator at the pointer position. The indicator is constrained in a disk between minimumRange and maximumRange around it's origin.
        /// </summary>
        /// <param name="indicator"> The indicator to position </param>
        /// <param name="origin"> The origin of the indicator (usually the caster) </param>
        /// <param name="minimumRange"></param>
        /// <param name="maximumRange"></param>
        protected void indicatorPosAtMouse ( Indicator indicator , Transform origin , float minimumRange , float maximumRange )
        {
            Vector3 worldPos = WorldManager.mouseWorldPos - origin.position;
            if ( !canTargetOverWalls )
            {
                RaycastHit hit;
                if ( Physics.Raycast ( spell.lanceur.transform.position , WorldManager.mouseWorldPos - spell.lanceur.transform.position , out hit , Vector3.Distance ( spell.lanceur.transform.position , WorldManager.mouseWorldPos ) , 1 << SpellMetrics.obstacleLayer ) )
                    worldPos = hit.point - origin.position;
                worldPos = worldPos + ( worldPos - origin.position ).normalized * SpellMetrics.indicatorWallOffset;
            }

            Vector3 allowedPos = clampMagnitude ( worldPos , minimumRange , maximumRange ) + origin.position;
            indicator.transform.position = allowedPos;
        }

        /// <summary>
        /// Place the indicator at the pointer position. The indicator is constrained in a disk between minimumRange and maximumRange around it's origin.
        /// </summary>
        /// <param name="indicator"> The indicator to position </param>
        /// <param name="origin"> The origin of the indicator (usually the caster) </param>
        /// <param name="minimumRange"></param>
        /// <param name="maximumRange"></param>
        protected void indicatorPosAtMouse ( Indicator indicator , Vector3 origin , float minimumRange , float maximumRange )
        {
            Vector3 worldPos = WorldManager.mouseWorldPos - origin;
            Vector3 allowedPos = clampMagnitude ( worldPos , minimumRange , maximumRange ) + origin;
            indicator.transform.position = allowedPos;
        }

        /// <summary>
        /// Place the indicator at the position of an object.
        /// </summary>
        /// <param name="indicator"> The indicator to position </param>
        /// <param name="transform"> The transform of the target object </param>
        protected void indicatorPosAtObject ( Indicator indicator , Transform transform )
        {
            indicator.transform.position = transform.position;
        }

        /// <summary>
        /// Place the indicator at a position.
        /// </summary>
        /// <param name="indicator"> The indicator to position </param>
        /// <param name="pos"> The position where the indicator will be put </param>
        protected void indicatorPosAtVector3 ( Indicator indicator , Vector3 pos )
        {
            indicator.transform.position = pos;
        }

        /// <summary>
        /// Rotate the indicator toward the mouse pointer.
        /// </summary>
        /// <param name="indicator"> The indicator to rotate </param>
        /// <param name="origin"> The origin of the indicator (usually the caster) </param>
        protected void indicatorLookAtMouse ( Indicator indicator , Transform origin )
        {
            Vector3 worldPos = WorldManager.mouseWorldPos;// - origin.position;
            indicator.transform.LookAt ( new Vector3 ( worldPos.x , 0 , worldPos.z ) );
        }

        /// <summary>
        /// Sets the localScale of the indicator
        /// </summary>
        /// <param name="indicator"> The indicator to sets </param>
        /// <param name="radius"> The wanted radius </param>
        protected void indicatorRadius ( Indicator indicator , float radius )
        {
            indicator.transform.localScale = radius * Vector3.one * 2;
        }

        /// <summary>
        /// Sets the radius of the sphereCollider
        /// </summary>
        /// <param name="indicator"> The sphereCollider to sets </param>
        /// <param name="radius"> The wanted radius </param>
        protected void indicatorSphereColliderRadius ( Indicator indicator , float radius )
        {
            indicator.GetComponentInChildren<SphereCollider> ().radius = radius;
        }

        /// <summary>
        /// Returns the collisions of an indicator.
        /// </summary>
        /// <param name="indicator"> The indicator from wich the collisions are retrived </param>
        /// <returns></returns>
        protected List<GameObject> acquisitionCibles ( Indicator indicator )
        {
            return indicator.GetComponentInChildren<ColliderInfos> ().getCollisions ();
        }

        /// <summary>
        /// Return a Vector3 clamped inside two disks.
        /// </summary>
        /// <param name="vector"> The position to Clamp </param>
        /// <param name="min"> The smaller of the two disks </param>
        /// <param name="max"> The bigger of the two disks </param>
        /// <returns></returns>
        protected Vector3 clampMagnitude ( Vector3 vector , float min , float max )
        {
            double sm = vector.sqrMagnitude;
            if ( sm > max * max ) return vector.normalized * max;
            else if ( sm < min * min ) return vector.normalized * min;

            return vector;
        }

        /// <summary>
        /// Return true if the target is valid according to the strategy parameters.
        /// </summary>
        /// <param name="target"> The object to check </param>
        /// <returns></returns>
        protected virtual bool isValidTarget ( GameObject target , Vector3 origin )
        {
            if ( null == target )
                return false;
            if ( target.tag != "Character" )
                return false;
            if ( ReferenceEquals ( target , spell.lanceur.gameObject ) && !canHitUser )
                return false;
            if ( TeamManager.getInstance ().compareTeam ( spell.lanceur , target.GetComponent<Character> () ) && !canHitTeam )
                return false;
            if ( !TeamManager.getInstance ().compareTeam ( spell.lanceur , target.GetComponent<Character> () ) && !canHitEnemies )
                return false;
            if ( !canHitThroughWalls )
                if ( checkForWallBetweenObjects ( origin , target.transform.position ) )
                    return false;
                    
            return true;
        }

        /// <summary>
        /// Return true of their is an Obstacle between the origin and the target.
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        protected virtual bool checkForWallBetweenObjects (Vector3 origin, Vector3 target)
        {
            RaycastHit hit;
            if ( Physics.Raycast ( origin , target - origin , out hit , Vector3.Distance ( origin , target ) , 1 << SpellMetrics.obstacleLayer ) )
                return true;
            return false;
        }

        /// <summary>
        /// Hide and reset the indicators.
        /// </summary>
        public void unsetIndicator ()
        {
            resetLists ();
            startHide ();
            reset ();
        }

        /// <summary>
        /// Hide the indicators.
        /// </summary>
        public void startHide ()
        {
            isHidden = true;
            hide ();
        }

        /// <summary>
        /// Unhide the indicators.
        /// </summary>
        public void startUnhide ()
        {
            isHidden = false;
            unHide ();
        }

        /// <summary>
        /// Empty the lists of valids and finals indiscators in the strategy.
        /// </summary>
        protected virtual void resetLists ()
        {
            foreach ( var obj in validIndicators )
                Destroy ( obj.gameObject );
            foreach ( var obj in finalIndicators )
                Destroy ( obj.gameObject );
            validIndicators = new List<Indicator> ();
            finalIndicators = new List<Indicator> ();
        }
    }
}