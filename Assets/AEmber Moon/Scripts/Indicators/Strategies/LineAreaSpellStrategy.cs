﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class LineAreaSpellStrategy : IndicatorStrategy
    {
        [Header ( "Area parameters" )]
        protected Indicator rangeIndicator;
        protected Indicator minRangeIndicator;

        public float range;
        public float radius;
        public float minimumRange;
        public float maximumRange;

        [Header ( "Line parameters" )]
        protected Indicator lineIndicator;
        protected List<Indicator> validLines = new List<Indicator> ();
        protected List<Indicator> finalLines = new List<Indicator> ();
        public float width;

        public override void init ()
        {
            indicator = SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.Area );
            rangeIndicator = SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.Range );
            minRangeIndicator = SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.MinRange );
            lineIndicator = SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.Line );
        }

        public override void setUp ()
        {
            indicatorPosAtObject ( lineIndicator , spell.lanceur.transform );
            indicatorPosAtObject ( minRangeIndicator , spell.lanceur.transform );
            indicatorPosAtObject ( rangeIndicator , spell.lanceur.transform );
            indicatorRadius ( rangeIndicator , maximumRange );
            indicatorRadius ( minRangeIndicator , minimumRange );
            indicatorRadius ( indicator , radius );
        }

        protected override void validUpdate ()
        {
            indicatorPosAtMouse ( indicator , spell.lanceur.transform , minimumRange , maximumRange );
            indicatorLookAtMouse ( lineIndicator , spell.lanceur.transform );
            float distance = Vector3.Distance ( indicator.transform.position , spell.lanceur.transform.position );
            lineIndicator.GetComponent<LineIndicator> ().setUp ( distance , width );
        }

        protected override void hide ()
        {
            indicator.SetActive ( false );
            rangeIndicator.SetActive ( false );
            minRangeIndicator.SetActive ( false );
            lineIndicator.SetActive ( false );
        }

        protected override void unHide ()
        {
            indicator.SetActive ( true );
            rangeIndicator.SetActive ( true );
            if ( minimumRange > 0 )
                minRangeIndicator.SetActive ( true );
            lineIndicator.SetActive ( true );
        }

        public override void reset ()
        {
            for ( int i = finalIndicators.Count - 1 ; i >= 0 ; i-- )
                Destroy ( finalIndicators[ i ] );
            finalIndicators = new List<Indicator> ();
        }

        public override void oneTargeting ()
        {
            finalLines.Add ( Instantiate ( lineIndicator ) );
            base.oneTargeting ();
        }

        public override void multipleTargetings ()
        {
            validLines.Add ( Instantiate ( lineIndicator ) );
            if ( validLines.Count == numberOfTargetings )
            {
                finalLines.AddRange ( validLines );
                validLines = new List<Indicator> ();
            }
            base.multipleTargetings ();
        }

        protected override List<IndicatorTarget> getAllTargets ()
        {
            List<IndicatorTarget> targets = new List<IndicatorTarget> ();

            foreach ( KeyValuePair<GameObject , float> target in getTargets ( indicator ) )
                targets.Add ( new IndicatorTarget ( target , 1 ) );
            foreach ( KeyValuePair<GameObject , float> target in getTargets ( lineIndicator ) )
                if ( !canHitMultipleTimes )
                {
                    bool add = true;
                    foreach ( IndicatorTarget tar in targets )
                        if ( tar.target == target.Key )
                            add = false;
                    if ( add )
                        targets.Add ( new IndicatorTarget ( target , 2 ) );
                }
                else
                    targets.Add ( new IndicatorTarget ( target , 2 ) );

            return targets;
        }

        public override List<IndicatorData> getPositionsAndRotationsFromIndicators ()
        {
            List<IndicatorData> positions = new List<IndicatorData> ();
            for ( int i = 0 ; i < finalIndicators.Count ; i++ )
                positions.Add ( new IndicatorData ( new List<Vector3> () { finalLines[ i ].transform.position , finalIndicators[ i ].transform.position } , finalLines[ i ].transform.rotation ) );

            return positions;
        }

        protected override void editorWarning ()
        {
            foreach ( MonoGenericEffect effect in GetComponents<MonoGenericEffect> () )
                foreach ( int i in effect.indicatorIndexes )
                    if ( i > 2 )
                    {
                        Debug.LogWarning ( "Error at spell " + GetComponent<GenericSpell> ().abilityName + ". One or more effects have out of bonds IndicatorIndexes for this IndicatorStratedy (" + GetType ().Name + "). The higer value should be '2'" );
                        return;
                    }
        }

        protected override void resetLists ()
        {
            base.resetLists ();
            foreach ( var obj in validLines )
                Destroy ( obj.gameObject );
            foreach ( var obj in finalLines )
                Destroy ( obj.gameObject );
            validLines = new List<Indicator> ();
            finalLines = new List<Indicator> ();
        }
    }
}