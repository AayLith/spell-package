﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class ProjectileIndicatorAdaptativeStrategy : LineIndicatorAdaptativeStrategy
    {
        protected override void validUpdate ()
        {
            base.validUpdate ();

            for ( int i = 0 ; i < numberOfLines ; i++ )
            {
                float distance = getClosestTarget ( getTargets ( indicator.transform.GetChild ( i ).GetComponent<Indicator> () ) ).Value;
                if ( distance < length )
                    transform.GetChild ( i ).GetComponent<LineIndicator> ().setUp ( distance , width );
                else
                    transform.GetChild ( i ).GetComponent<LineIndicator> ().setUp ( length , width );
            }
        }
    }
}