﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class LineIndicatorAdaptativeStrategy : LineIndicatorStrategy
    {
        public float maxSpread;

        [Header ( "Mouse" )]
        public float minSpreadMouseDistance;
        public float maxSpreadMouseDistance;

        protected override void validUpdate ()
        {
            indicatorLookAtMouse ( indicator , spell.lanceur.transform );

            Vector3 worldPos = WorldManager.mouseWorldPos;
            float distance = Vector3.Distance ( spell.lanceur.transform.position , worldPos );
            float spreadFactor = Mathf.InverseLerp ( minSpreadMouseDistance , maxSpreadMouseDistance , distance );
            float rotationAmount = Mathf.Max ( maxSpread / ( numberOfLines - 1 ) * spreadFactor , spread / ( numberOfLines - 1 ) );
            float rotation = Mathf.Min ( -maxSpread / 2 * spreadFactor , -spread / 2 );
            for ( int i = 0 ; i < numberOfLines ; i++ )
            {
                indicator.transform.GetChild ( i ).transform.localRotation = Quaternion.Euler ( new Vector3 ( 0 , rotation , 0 ) );
                rotation += rotationAmount;
            }
        }
    }
}