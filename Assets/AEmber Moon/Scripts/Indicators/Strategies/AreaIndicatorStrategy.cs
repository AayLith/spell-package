﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class AreaIndicatorStrategy : IndicatorStrategy
    {
        [Header ( "Area parameters" )]
        protected Indicator rangeIndicator;
        protected Indicator minRangeIndicator;
        public float radius;
        public float minimumRange;
        public float maximumRange;

        public override void init ()
        {
            indicator = SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.Area );
            rangeIndicator = SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.Range );
            if ( minimumRange > 0 )
                minRangeIndicator = SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.MinRange );
        }

        public override void setUp ()
        {
            indicatorRadius ( indicator , radius );
            indicatorPosAtObject ( rangeIndicator , spell.lanceur.transform );
            indicatorRadius ( rangeIndicator , maximumRange );
            if ( minimumRange > 0 )
            {
                indicatorPosAtObject ( minRangeIndicator , spell.lanceur.transform );
                indicatorRadius ( minRangeIndicator , minimumRange );
            }
        }

        protected override void validUpdate ()
        {
            indicatorPosAtMouse ( indicator , spell.lanceur.transform , minimumRange , maximumRange );
        }

        protected override void hide ()
        {
            indicator.SetActive ( false );
            rangeIndicator.SetActive ( false );
            minRangeIndicator.SetActive ( false );
        }

        protected override void unHide ()
        {
            indicator.SetActive ( true );
            rangeIndicator.SetActive ( true );
            minRangeIndicator.SetActive ( true );
        }

        public override void reset ()
        {
            for ( int i = finalIndicators.Count - 1 ; i >= 0 ; i-- )
                Destroy ( finalIndicators[ i ] );
            finalIndicators = new List<Indicator> ();
        }

        protected override List<IndicatorTarget> getAllTargets ()
        {
            List<IndicatorTarget> targets = new List<IndicatorTarget> ();

            foreach ( KeyValuePair<GameObject , float> target in getTargets ( indicator ) )
                targets.Add ( new IndicatorTarget ( target , 1 ) );

            return targets;
        }

        public override List<IndicatorData> getPositionsAndRotationsFromIndicators ()
        {
            List<IndicatorData> positions = new List<IndicatorData> ();
            for ( int i = 0 ; i < finalIndicators.Count ; i++ )
                positions.Add ( new IndicatorData ( new List<Vector3> () { finalIndicators[ i ].transform.position } , Quaternion.LookRotation ( finalIndicators[ i ].transform.rotation.eulerAngles ) ) );

            return positions;
        }

        protected override void editorWarning ()
        {
            foreach ( MonoGenericEffect effect in GetComponents<MonoGenericEffect> () )
                if ( !ArrayContains.contains ( effect.indicatorIndexes , ( byte ) 1 ) )
                {
                    Debug.LogWarning ( "Error at spell " + GetComponent<GenericSpell> ().abilityName + ". One or more effects have out of bonds IndicatorIndexes for this IndicatorStratedy (" + GetType ().Name + "). The only value should be '1'" );
                    return;
                }
        }
    }
}