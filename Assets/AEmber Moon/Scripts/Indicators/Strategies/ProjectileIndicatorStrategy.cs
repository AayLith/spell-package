﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class ProjectileIndicatorStrategy : LineIndicatorStrategy
    {
        protected override void validUpdate ()
        {
            base.validUpdate ();

            foreach ( Transform trans in indicator.transform )
            {
                var target = getClosestTarget ( getTargets ( trans.GetComponent<Indicator> () ) );
                if ( target.Key )
                {
                    if ( target.Value < length )
                        trans.GetComponent<LineIndicator> ().setUpWithoutCollider ( target.Value , width );
                }
                else
                    trans.GetComponent<LineIndicator> ().setUp ( length , width );
            }
        }

        protected override List<IndicatorTarget> getAllTargets ()
        {
            List<IndicatorTarget> targets = new List<IndicatorTarget> ();

            foreach ( Transform trans in indicator.transform )
            {
                var target = getClosestTarget ( getTargets ( trans.GetComponent<Indicator> () ) );
                if ( target.Key )
                    if ( !canHitMultipleTimes )
                    {
                        bool add = true;
                        foreach ( IndicatorTarget tar in targets )
                            if ( tar.target == target.Key )
                                add = false;
                        if ( add )
                            targets.Add ( new IndicatorTarget ( target , 1 ) );
                    }
                    else
                        targets.Add ( new IndicatorTarget ( target , 1 ) );
            }

            return targets;
        }
    }
}