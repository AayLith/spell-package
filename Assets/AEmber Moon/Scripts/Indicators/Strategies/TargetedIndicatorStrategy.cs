﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class TargetedIndicatorStrategy : IndicatorStrategy
    {
        public float maximumRange;
        public float minimumRange;
        public float searchRadius;
        protected Indicator rangeIndicator;
        protected Indicator minRangeIndicator;
        protected Indicator spherecollider;

        [Header ( "Line Parameters" )]
        protected Indicator line;
        protected List<Indicator> validLines = new List<Indicator> ();
        protected List<Indicator> finalLines = new List<Indicator> ();
        public float lineStartHeight;
        [Range ( 0 , 1 )]
        public float lineApexLocation;
        public float lineApexHeight;

        protected KeyValuePair<GameObject , float> currentTarget = new KeyValuePair<GameObject , float> ();

        public override void init ()
        {
            rangeIndicator = SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.Range );
            minRangeIndicator = SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.MinRange );
            line = SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.CurvedLine );
            spherecollider = SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.SphereCollider );
        }

        public override void setUp ()
        {
            spherecollider.GetComponent<SphereCollider> ().radius = searchRadius;
            indicatorRadius ( rangeIndicator , maximumRange );
            indicatorRadius ( minRangeIndicator , minimumRange );
        }

        protected override void hide ()
        {
            rangeIndicator.SetActive ( false );
            minRangeIndicator.SetActive ( false );
            line.SetActive ( false );
            spherecollider.SetActive ( false );
        }

        protected override void unHide ()
        {
            rangeIndicator.SetActive ( true );
            minRangeIndicator.SetActive ( true );
            line.SetActive ( true );
            spherecollider.SetActive ( true );
        }

        protected override void validUpdate ()
        {
            indicatorPosAtMouse ( spherecollider , spell.lanceur.transform , minimumRange , maximumRange );

            currentTarget = getClosestTarget ( getTargets ( spherecollider ) );

            if ( currentTarget.Key && currentTarget.Value <= maximumRange )
            {
                if ( !isHidden )
                    line.SetActive ( true );
                line.transform.position = spell.lanceur.transform.position;
                line.transform.GetChild ( 0 ).position = spell.lanceur.transform.position + Vector3.up * lineStartHeight;
                line.transform.GetChild ( 2 ).position = getClosestTarget ( getTargets ( spherecollider ) ).Key.transform.position;
                line.transform.GetChild ( 1 ).position = new Vector3 (
                    ( line.transform.GetChild ( 0 ).position.x + line.transform.GetChild ( 2 ).position.x ) * lineApexLocation ,
                    lineApexHeight ,
                    ( line.transform.GetChild ( 0 ).position.z + line.transform.GetChild ( 2 ).position.z ) * lineApexLocation );
            }
            else
            {
                line.SetActive ( false );
            }
        }

        public override void reset ()
        {
            for ( int i = finalIndicators.Count - 1 ; i >= 0 ; i-- )
            {
                Destroy ( finalIndicators[ i ] );
                Destroy ( finalLines[ i ] );
            }
            finalIndicators = new List<Indicator> ();
            finalLines = new List<Indicator> ();
        }

        public override void goBack ()
        {
            base.goBack ();
            Destroy ( validLines[ validIndicators.Count - 1 ] );
            validLines.RemoveAt ( validIndicators.Count - 1 );
        }

        public override void oneTargeting ()
        {
            finalIndicators.Add ( new GameObject ().AddComponent<Indicator> () );
            finalLines.Add ( Instantiate ( line ) );
            spell.validateSpell ( getAllTargets () );
        }

        public override void multipleTargetings ()
        {
            validLines.Add ( Instantiate ( line ) );
            validIndicators.Add ( new GameObject ().AddComponent<Indicator> () );
            if ( validLines.Count == numberOfTargetings )
            {
                finalIndicators.AddRange ( validIndicators );
                finalLines.AddRange ( validLines );
                validLines = new List<Indicator> ();
                validIndicators = new List<Indicator> ();
            }
            base.multipleTargetings ();
        }

        protected override List<IndicatorTarget> getAllTargets ()
        {
            return new List<IndicatorTarget> () { new IndicatorTarget ( getClosestTarget ( getTargets ( spherecollider ) ) , 1 ) };
        }

        public override List<IndicatorData> getPositionsAndRotationsFromIndicators ()
        {
            List<IndicatorData> positions = new List<IndicatorData> ();
            for ( int i = 0 ; i < finalLines.Count ; i++ )
            {
                Vector3[] points = new Vector3[ finalLines[ i ].GetComponent<LineRenderer> ().positionCount ];
                finalLines[ i ].GetComponent<LineRenderer> ().GetPositions ( points );
                List<Vector3> list = new List<Vector3> ();
                list.AddRange ( points );
                positions.Add ( new IndicatorData ( list , Quaternion.LookRotation ( finalIndicators[ i ].transform.rotation.eulerAngles ) ) );
            }

            return positions;
        }

        protected override void editorWarning ()
        {
            foreach ( MonoGenericEffect effect in GetComponents<MonoGenericEffect> () )
                if ( !ArrayContains.contains ( effect.indicatorIndexes , ( byte ) 1 ) )
                {
                    Debug.LogWarning ( "Error at spell " + GetComponent<GenericSpell> ().abilityName + ". One or more effects have out of bonds IndicatorIndexes for this IndicatorStratedy (" + GetType ().Name + "). The only value should be '1'" );
                    return;
                }
        }

        protected override void resetLists ()
        {
            base.resetLists ();
            foreach ( var obj in validLines )
                Destroy ( obj.gameObject );
            foreach ( var obj in finalLines )
                Destroy ( obj.gameObject );
            validLines = new List<Indicator> ();
            finalLines = new List<Indicator> ();
        }
    }
}