﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class LineIndicatorStrategy : IndicatorStrategy
    {
        [Header ( "Line parameters" )]
        public float width;
        public float length;
        public float spread;
        public int numberOfLines;

        public override void OnValidate ()
        {
            base.OnValidate ();

            numberOfLines = Mathf.Max ( 1 , numberOfLines );
            spread = Mathf.Max ( 0 , spread );
            length = Mathf.Max ( 0 , length );
            width = Mathf.Max ( 0 , width );
        }

        public override void init ()
        {
            indicator = new GameObject ().AddComponent<Indicator> ();
            indicator.name = "Line Indicator ...";
            indicator.transform.SetParent ( WorldManager.getInstance ().transform );
            for ( int i = 0 ; i < numberOfLines ; i++ )
            {
                var obj = Instantiate ( SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.Line ) , indicator.transform );
                obj.SetActive ( true );
            }
        }

        public override void setUp ()
        {
            indicatorPosAtObject ( indicator , spell.lanceur.transform );
            foreach ( Transform line in indicator.transform )
            {
                line.GetComponent<LineIndicator> ().setUp ( length , width );
                line.GetComponentInChildren<ColliderInfos> ().resetColliders ();
            }
        }

        protected override void hide ()
        {
            indicator.SetActive ( false );
        }

        protected override void unHide ()
        {
            indicator.gameObject.SetActive ( true );
        }

        protected override void validUpdate ()
        {
            indicatorLookAtMouse ( indicator , spell.lanceur.transform );

            float rotationAmount = spread / ( numberOfLines - 1 );
            float rotation = -spread / 2;
            for ( int i = 0 ; i < numberOfLines ; i++ )
            {
                indicator.transform.GetChild ( i ).transform.localRotation = Quaternion.Euler ( new Vector3 ( 0 , rotation , 0 ) );
                rotation += rotationAmount;
            }
        }

        public override void reset ()
        {
            for ( int i = finalIndicators.Count - 1 ; i >= 0 ; i-- )
                Destroy ( finalIndicators[ i ] );
            finalIndicators = new List<Indicator> ();
        }

        protected override List<IndicatorTarget> getAllTargets ()
        {
            List<IndicatorTarget> targets = new List<IndicatorTarget> ();

            foreach ( Transform trans in indicator.transform )
                foreach ( KeyValuePair<GameObject , float> target in getTargets ( trans.GetComponent<Indicator> () ) )
                    if ( !canHitMultipleTimes )
                    {
                        bool add = true;
                        foreach ( IndicatorTarget tar in targets )
                            if ( tar.target == target.Key )
                                add = false;
                        if ( add )
                            targets.Add ( new IndicatorTarget ( target , 1 ) );
                    }
                    else
                        targets.Add ( new IndicatorTarget ( target , 1 ) );

            return targets;
        }

        public override List<IndicatorData> getPositionsAndRotationsFromIndicators ()
        {
            List<IndicatorData> positions = new List<IndicatorData> ();
            for ( int j = 0 ; j < finalIndicators.Count ; j++ )
                for ( int i = 0 ; i < finalIndicators[ j ].transform.childCount ; i++ )
                    positions.Add ( new IndicatorData ( new List<Vector3> () { finalIndicators[ i ].transform.position } , finalIndicators[ i ].transform.GetChild ( i ).transform.rotation ) );

            return positions;
        }

        protected override void editorWarning ()
        {
            foreach ( MonoGenericEffect effect in GetComponents<MonoGenericEffect> () )
                if ( !ArrayContains.contains ( effect.indicatorIndexes , ( byte ) 1 ) )
                {
                    Debug.LogWarning ( "Error at spell " + GetComponent<GenericSpell> ().abilityName + ". One or more effects have out of bonds IndicatorIndexes for this IndicatorStratedy (" + GetType ().Name + "). The only value should be '1'" );
                    return;
                }
        }
    }
}