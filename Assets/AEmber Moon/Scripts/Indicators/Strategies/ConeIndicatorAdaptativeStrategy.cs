﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class ConeIndicatorAdaptativeStrategy : ConeIndicatorStrategy
    {
        public float minAngle;
        public float maxAngle;
        public float minRadius;
        public float maxRadius;

        [Header ( "Mouse" )]
        public float minSpreadMouseDistance;
        public float maxSpreadMouseDistance;

        public float currentAngle { get; private set; }

        public override void OnValidate ()
        {
            base.OnValidate ();

            minAngle = Mathf.Max ( minAngle , 0 );
            minRadius = Mathf.Max ( minRadius , 0 );
            maxAngle = Mathf.Max ( minAngle , maxAngle );
            maxRadius = Mathf.Max ( minRadius , maxRadius );
            maxSpreadMouseDistance = Mathf.Max ( maxSpreadMouseDistance , 0 );
            minSpreadMouseDistance = Mathf.Max ( minSpreadMouseDistance , 0 );
        }

        public override void setUp ()
        {
            indicatorPosAtObject ( indicator , spell.lanceur.transform );
            for ( int i = 0 ; i < indicator.transform.childCount ; i++ )
                indicator.transform.GetChild ( i ).gameObject.SetActive ( true );
            foreach ( Transform cone in indicator.transform )
            {
                cone.GetComponentInChildren<DrawCones> ( true ).setUp ( numberOfCones );
                cone.GetComponentInChildren<Collider> ().GetComponent<ColliderInfos> ().resetColliders ();
                cone.GetComponentInChildren<DrawCones> ( true ).drawConcentricCones ( angle , new float[] { radius } );
            }
        }

        protected override void validUpdate ()
        {
            indicatorLookAtMouse ( indicator , spell.lanceur.transform );
            indicator.transform.position = new Vector3 ( indicator.transform.position.x , 0.01f , indicator.transform.position.z );

            Vector3 worldPos = WorldManager.mouseWorldPos;

            float rotationAmount = spread / ( numberOfCones - 1 );
            float rotation = -spread / 2;
            float distance = Vector3.Distance ( spell.lanceur.transform.position , worldPos );

            for ( int i = 0 ; i < numberOfCones ; i++ )
            {
                indicator.transform.GetChild ( i ).transform.localRotation = Quaternion.Euler ( new Vector3 ( 90 , rotation , 0 ) );
                rotation += rotationAmount;
            }

            if ( distance > maxSpreadMouseDistance )
            {
                indicator.GetComponentInChildren<DrawCones> ( true ).drawConcentricCones ( minAngle , new float[] { maxRadius } );
                indicator.GetComponentInChildren<SphereCollider> ().radius = maxRadius;
                currentAngle = minAngle;
            }
            else if ( distance < minSpreadMouseDistance )
            {
                indicator.GetComponentInChildren<DrawCones> ( true ).drawConcentricCones ( maxAngle , new float[] { minRadius } );
                currentAngle = maxAngle;
            }
            else
            {
                currentAngle = maxAngle - ( ( maxAngle - minAngle ) * Mathf.InverseLerp ( minRadius , maxRadius , distance ) ); //((distance - minConeLength) / (maxConeLength-minConeLength)));
                indicator.GetComponentInChildren<DrawCones> ( true ).drawConcentricCones ( currentAngle , new float[] { distance } );
            }
        }
    }
}