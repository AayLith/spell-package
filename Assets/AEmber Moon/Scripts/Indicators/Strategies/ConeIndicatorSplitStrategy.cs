﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class ConeIndicatorSplitStrategy : ConeIndicatorStrategy
    {
        public float maxSpread;

        [Header ( "Mouse" )]
        public float minSpreadMouseDistance;
        public float maxSpreadMouseDistance;

        public override void OnValidate ()
        {
            base.OnValidate ();

            maxSpreadMouseDistance = Mathf.Max ( maxSpreadMouseDistance , 0 );
            minSpreadMouseDistance = Mathf.Max ( minSpreadMouseDistance , 0 );
            maxSpread = Mathf.Max ( maxSpread , 0 );
        }

        protected override void validUpdate ()
        {
            indicatorLookAtMouse ( indicator , spell.lanceur.transform );

            Vector3 worldPos = WorldManager.mouseWorldPos;
            float distance = Vector3.Distance ( spell.lanceur.transform.position , worldPos );
            float spreadFactor = Mathf.InverseLerp ( minSpreadMouseDistance , maxSpreadMouseDistance , distance );
            float rotationAmount = Mathf.Max ( maxSpread / ( numberOfCones - 1 ) * spreadFactor , spread / ( numberOfCones - 1 ) );
            float rotation = Mathf.Min ( -maxSpread / 2 * spreadFactor , -spread / 2 );
            for ( int i = 0 ; i < numberOfCones ; i++ )
            {
                indicator.transform.GetChild ( i ).transform.localRotation = Quaternion.Euler ( new Vector3 ( 90 , rotation , 0 ) );
                rotation += rotationAmount;
            }
        }
    }
}