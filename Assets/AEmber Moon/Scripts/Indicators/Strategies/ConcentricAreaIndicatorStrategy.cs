﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Æmber_Moon
{
    public class ConcentricAreaIndicatorStrategy : IndicatorStrategy
    {
        [Header ( "Area parameters" )]
        protected Indicator rangeIndicator;
        protected Indicator minRangeIndicator;

        [Tooltip ( "A utiliser pour set le Array" )]
        public int numberOfCircles;
        [Tooltip ( "From the inside to the outside" )]
        public float[] radiuses;
        public float minimumRange;
        public float maximumRange;

        public override void init ()
        {
            indicator = new GameObject ().AddComponent<Indicator> ();
            indicator.transform.SetParent ( WorldManager.getInstance ().transform );
            Instantiate ( SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.Area ) , indicator.transform );
            for ( int i = 1 ; i < numberOfCircles ; i++ )
            {
                Instantiate ( SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.Circle ) , indicator.transform );
            }
            rangeIndicator = SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.Range );
            minRangeIndicator = SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.MinRange );
        }

        public override void OnValidate ()
        {
            base.OnValidate ();

            numberOfCircles = Mathf.Max ( numberOfCircles , 1 );

            float[] tempRad = radiuses;
            Array.Sort ( tempRad );

            radiuses = new float[ numberOfCircles ];

            for ( int i = 0 ; i < numberOfCircles && i < tempRad.Length ; i++ )
                radiuses[ i ] = tempRad[ i ];
        }

        public override void setUp ()
        {
            for ( int i = numberOfCircles - 1 ; i >= 0 ; i-- )
                indicatorRadius ( indicator.transform.GetChild ( i ).GetComponent<Indicator> () , radiuses[ numberOfCircles - 1 - i ] );
            indicatorPosAtObject ( rangeIndicator , spell.lanceur.transform );
            indicatorRadius ( rangeIndicator , maximumRange );
            if ( minimumRange > 0 )
            {
                indicatorPosAtObject ( minRangeIndicator , spell.lanceur.transform );
                indicatorRadius ( minRangeIndicator , maximumRange );
            }
        }

        protected override void validUpdate ()
        {
            indicatorPosAtMouse ( indicator , spell.lanceur.transform , minimumRange , maximumRange );
        }

        protected override void hide ()
        {
            indicator.SetActive ( false );
            rangeIndicator.SetActive ( false );
            minRangeIndicator.SetActive ( false );
        }

        protected override void unHide ()
        {
            indicator.gameObject.SetActive ( true );
            for ( int i = 0 ; i < indicator.transform.childCount ; i++ )
                indicator.transform.GetChild ( i ).gameObject.SetActive ( true );
            rangeIndicator.SetActive ( true );
            if ( minimumRange > 0 )
                minRangeIndicator.SetActive ( true );
        }

        public override void reset ()
        {
            for ( int i = finalIndicators.Count - 1 ; i >= 0 ; i-- )
                Destroy ( finalIndicators[ i ] );
            finalIndicators = new List<Indicator> ();
        }

        protected override List<IndicatorTarget> getAllTargets ()
        {
            List<IndicatorTarget> targets = new List<IndicatorTarget> ();

            foreach ( KeyValuePair<GameObject , float> target in getTargets ( indicator ) )
            {
                for ( int i = 0 ; i < numberOfCircles ; i++ )
                    if ( target.Value <= radiuses[ i ] )
                    {
                        targets.Add ( new IndicatorTarget ( target , ( byte ) ( i + 1 ) ) );
                        break;
                    }
            }

            return targets;
        }

        public override List<IndicatorData> getPositionsAndRotationsFromIndicators ()
        {
            List<IndicatorData> positions = new List<IndicatorData> ();
            for ( int i = 0 ; i < finalIndicators.Count ; i++ )
                positions.Add ( new IndicatorData ( new List<Vector3> () { finalIndicators[ i ].transform.position } , Quaternion.LookRotation ( finalIndicators[ i ].transform.rotation.eulerAngles ) ) );

            return positions;
        }

        protected override void editorWarning ()
        {
            foreach ( MonoGenericEffect effect in GetComponents<MonoGenericEffect> () )
                foreach ( int i in effect.indicatorIndexes )
                    if ( i > numberOfCircles )
                    {
                        Debug.LogWarning ( "Error at spell " + GetComponent<GenericSpell> ().abilityName + ". One or more effects have out of bonds IndicatorIndexes for this IndicatorStratedy (" + GetType ().Name + "). The higer value should be 'numberOfCircles'" );
                        return;
                    }
        }
    }
}