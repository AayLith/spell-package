﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class ConcentricAreaCurvedLineStrategy : ConcentricAreaIndicatorStrategy
    {
        [Header ( "Line Parameters" )]
        protected Indicator line;
        protected List<Indicator> validLines = new List<Indicator> ();
        protected List<Indicator> finalLines = new List<Indicator> ();
        public float lineStartHeight;
        [Range ( 0 , 1 )]
        public float lineApexLocation;
        public float lineApexHeight;

        public override void init ()
        {
            base.init ();
            line = SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.CurvedLine );
        }

        protected override void validUpdate ()
        {
            base.validUpdate ();

            line.transform.position = spell.lanceur.transform.position;
            line.transform.GetChild ( 0 ).position = spell.lanceur.transform.position + Vector3.up * lineStartHeight;
            line.transform.GetChild ( 2 ).position = indicator.transform.position;
            line.transform.GetChild ( 1 ).position = new Vector3 (
                ( line.transform.GetChild ( 0 ).position.x + line.transform.GetChild ( 2 ).position.x ) * lineApexLocation ,
                lineApexHeight ,
                ( line.transform.GetChild ( 0 ).position.z + line.transform.GetChild ( 2 ).position.z ) * lineApexLocation );
        }

        protected override void hide ()
        {
            base.hide ();
            line.SetActive ( false );
        }

        protected override void unHide ()
        {
            base.unHide ();
            line.SetActive ( true );
        }

        public override void reset ()
        {
            for ( int i = finalIndicators.Count - 1 ; i >= 0 ; i-- )
            {
                Destroy ( finalIndicators[ i ] );
                Destroy ( finalLines[ i ] );
            }
            finalIndicators = new List<Indicator> ();
            finalLines = new List<Indicator> ();
        }

        public override void goBack ()
        {
            base.goBack ();
            Destroy ( validLines[ validIndicators.Count - 1 ] );
            validLines.RemoveAt ( validIndicators.Count - 1 );
        }

        public override void oneTargeting ()
        {
            finalLines.Add ( Instantiate ( line ) );
            base.oneTargeting ();
        }

        public override void multipleTargetings ()
        {
            validLines.Add ( Instantiate ( line ) );
            if ( validLines.Count == numberOfTargetings )
            {
                finalLines.AddRange ( validLines );
                validLines = new List<Indicator> ();
            }
            base.multipleTargetings ();
        }

        public override List<IndicatorData> getPositionsAndRotationsFromIndicators ()
        {
            List<IndicatorData> positions = new List<IndicatorData> ();
            for ( int i = 0 ; i < finalIndicators.Count ; i++ )
            {
                Vector3[] points = new Vector3[ finalLines[ i ].GetComponent<LineRenderer> ().positionCount ];
                finalLines[ i ].GetComponent<LineRenderer> ().GetPositions ( points );
                List<Vector3> list = new List<Vector3> ();
                list.AddRange ( points );
                positions.Add ( new IndicatorData ( list , Quaternion.LookRotation ( finalIndicators[ i ].transform.rotation.eulerAngles ) ) );
            }
            return positions;
        }

        protected override void resetLists ()
        {
            base.resetLists ();
            foreach ( var obj in validLines )
                Destroy ( obj.gameObject );
            foreach ( var obj in finalLines )
                Destroy ( obj.gameObject );
            validLines = new List<Indicator> ();
            finalLines = new List<Indicator> ();
        }
    }
}