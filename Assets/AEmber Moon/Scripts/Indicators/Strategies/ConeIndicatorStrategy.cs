﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class ConeIndicatorStrategy : IndicatorStrategy
    {
        [Header ( "Cone parameters" )]
        public float radius;
        public float angle;
        public float spread;
        public int numberOfCones;

        public override void OnValidate ()
        {
            base.OnValidate ();

            numberOfCones = Mathf.Max ( numberOfCones , 1 );
            spread = Mathf.Max ( spread , 0 );
            angle = Mathf.Max ( angle , 0 );
            radius = Mathf.Max ( radius , 0 );
        }

        public override void init ()
        {
            indicator = new GameObject ().AddComponent<Indicator> ();
            indicator.transform.SetParent ( WorldManager.getInstance ().transform );
            indicator.name = "Cone Indicator";
            for ( int i = 0 ; i < numberOfCones ; i++ )
            {
                var obj = Instantiate ( SpellComponentsManager.getInstance ().getIndicator ( SpellMetrics.spellIndicators.Cone ) , indicator.transform );
                obj.SetActive ( true );
            }
        }

        public override void setUp ()
        {
            indicatorPosAtObject ( indicator , spell.lanceur.transform );
            foreach ( Transform obj in indicator.transform )
            {
                obj.GetComponent<DrawCones> ().setUp ( numberOfCones );
                obj.GetComponent<DrawCones> ().drawConcentricCones ( angle , new float[ 1 ] { radius } );
                obj.GetComponentInChildren<Collider> ().GetComponent<ColliderInfos> ().resetColliders ();
            }
        }

        protected override void validUpdate ()
        {
            indicatorLookAtMouse ( indicator , spell.lanceur.transform );

            float rotationAmount = spread / ( numberOfCones - 1 );
            float rotation = -spread / 2;
            for ( int i = 0 ; i < numberOfCones ; i++ )
            {
                indicator.transform.GetChild ( i ).transform.localRotation = Quaternion.Euler ( new Vector3 ( 90 , rotation , 0 ) );
                rotation += rotationAmount;
            }
        }

        protected override void hide ()
        {
            indicator.SetActive ( false );
        }

        protected override void unHide ()
        {
            indicator.gameObject.SetActive ( true );
            for ( int i = 0 ; i < indicator.transform.childCount ; i++ )
                indicator.transform.GetChild ( i ).gameObject.SetActive ( true );
        }

        public override void reset ()
        {
            for ( int i = finalIndicators.Count - 1 ; i >= 0 ; i-- )
                Destroy ( finalIndicators[ i ] );
            finalIndicators = new List<Indicator> ();
        }

        protected override List<IndicatorTarget> getAllTargets ()
        {
            List<IndicatorTarget> targets = new List<IndicatorTarget> ();

            foreach ( Transform trans in indicator.transform )
                foreach ( KeyValuePair<GameObject , float> target in getTargets ( trans.GetComponent<Indicator> () ) )
                    if ( verifIfTargetIsInCone ( target.Key , trans.GetComponent<Indicator> () ) )
                    {
                        if ( !canHitMultipleTimes )
                        {
                            bool add = true;
                            foreach ( IndicatorTarget tar in targets )
                                if ( tar.target == target.Key )
                                    add = false;
                            if ( add )
                                targets.Add ( new IndicatorTarget ( target , 1 ) );
                        }
                        else
                            targets.Add ( new IndicatorTarget ( target , 1 ) );
                    }

            return targets;
        }

        public override List<IndicatorData> getPositionsAndRotationsFromIndicators ()
        {
            List<IndicatorData> positions = new List<IndicatorData> ();
            for ( int j = 0 ; j < finalIndicators.Count ; j++ )
                for ( int i = 0 ; i < finalIndicators[ j ].transform.childCount ; i++ )
                    positions.Add ( new IndicatorData ( new List<Vector3> () { finalIndicators[ i ].transform.position } , finalIndicators[ i ].transform.GetChild ( i ).transform.rotation ) );

            return positions;
        }

        protected override void editorWarning ()
        {
            foreach ( MonoGenericEffect effect in GetComponents<MonoGenericEffect> () )
                if ( !ArrayContains.contains ( effect.indicatorIndexes , ( byte ) 1 ) )
                {
                    Debug.LogWarning ( "Error at spell " + GetComponent<GenericSpell> ().abilityName + ". One or more effects have out of bonds IndicatorIndexes for this IndicatorStratedy (" + GetType ().Name + "). The only value should be '1'" );
                    return;
                }
        }
    }
}