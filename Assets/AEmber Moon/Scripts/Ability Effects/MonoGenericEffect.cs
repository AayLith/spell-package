﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public abstract class MonoGenericEffect : MonoBehaviour, IGenericEffect
    {
        [HideInInspector]
        public Character source;
        public byte[] indicatorIndexes = new byte[ 1 ];
        public int duration;
        public Sprite sprite;

        public void OnValidate ()
        {
            if ( indicatorIndexes.Length > 0 )
                for ( int i = 0 ; i < indicatorIndexes.Length ; i++ )
                    indicatorIndexes[ i ] = ( byte ) Mathf.Max ( ( int ) indicatorIndexes[ i ] , 1 );
        }

        public abstract void applyEffect ( Character perso );

        public int getDuration ()
        {
            return duration;
        }

        public virtual void addToInspector ()
        {

        }

        public virtual void endOfTurnEffect ( Character perso )
        {

        }

        public virtual void startOfTurnEffect ( Character perso )
        {

        }

        public virtual void onTakeDamages ( Character perso )
        {

        }

        public virtual void onDealDamages ( Character perso )
        {

        }

        public abstract void previewEffect ( List<IndicatorTarget> targets );

        public abstract void undoPreviewEffect ();

        public abstract string getDescription ();
    }
}