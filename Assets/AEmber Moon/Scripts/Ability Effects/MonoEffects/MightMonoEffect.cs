﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class MightMonoEffect : MonoGenericEffect
    {
        public int amount;

        public override void applyEffect ( Character cible )
        {
            int index = ArrayContains.contains ( cible.effects , this.GetType () );
            if ( index < 0 )
                new MightEffect ( duration , amount , source , cible );
            else
                ( ( MightEffect ) cible.effects[ index ] ).addSource ( duration , amount , source );
        }

        public override string getDescription ()
        {
            return "Increase damages by " + amount + " for " + duration + " turns.";
        }

        public override void previewEffect ( List<IndicatorTarget> targets )
        {

        }

        public override void undoPreviewEffect ()
        {

        }
    }
}