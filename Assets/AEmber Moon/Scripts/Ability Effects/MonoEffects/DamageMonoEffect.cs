﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class DamageMonoEffect : MonoGenericEffect
    {
        public Damage damage;

        protected List<KeyValuePair<Character , int>> previewedDamages;

        private void Awake ()
        {
            previewedDamages = new List<KeyValuePair<Character , int>> ();
        }

        public DamageMonoEffect ( int damages , SpellMetrics.damageType damageType )
        {
            damage.amount = damages;
            damage.type = damageType;
            duration = 0;
        }

        public override void applyEffect ( Character perso )
        {
            perso.takeDamages ( source , damage );
        }

        public override void previewEffect ( List<IndicatorTarget> targets )
        {
            undoPreviewEffect ();

            foreach ( IndicatorTarget target in targets )
            {
                if ( ArrayContains.contains ( indicatorIndexes , target.indicatorIndex ) )
                {
                    int dmgBonus = source.GetComponent<Character> ().mitigateDealingDamages ( damage );
                    int dmgMalus = target.target.GetComponent<Character> ().mitigateReceivedDamages ( damage );

                    previewedDamages.Add ( new KeyValuePair<Character , int> ( target.target.GetComponent<Character> () , damage.amount + dmgBonus + dmgMalus ) );
                    target.target.GetComponent<Character> ().addDamagePreview ( damage.amount + dmgBonus + dmgMalus );
                }
            }
        }

        public override void undoPreviewEffect ()
        {
            for ( int i = 0 ; i < previewedDamages.Count ; i++ )
                previewedDamages[ i ].Key.addDamagePreview ( -previewedDamages[ i ].Value );
            previewedDamages.Clear ();
        }

        public override string getDescription ()
        {
            int dmg = GetComponent<GenericAbility> ().lanceur.mitigateDealingDamages ( damage );
            var text = damage.amount.ToString () + " (";
            if ( dmg >= 0 )
                text += '+';
            text += dmg + ") " + damage.type.ToString () + " damages.";
            return text;
        }
    }
}