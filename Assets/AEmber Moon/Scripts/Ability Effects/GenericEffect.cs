﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public abstract class GenericEffect : IGenericEffect
    {
        public int duration;

        public abstract void applyEffect ( Character perso );

        public int getDuration ()
        {
            return duration;
        }

        public virtual void addToInspector ()
        {

        }

        public virtual void endOfTurnEffect ( Character perso )
        {

        }

        public virtual void startOfTurnEffect ( Character perso )
        {

        }

        public virtual void onTakeDamages ( Character perso )
        {

        }

        public virtual void onDealDamages ( Character perso )
        {

        }

        public virtual int mitigateDamages ( int amount )
        {
            return 0;
        }
    }

}