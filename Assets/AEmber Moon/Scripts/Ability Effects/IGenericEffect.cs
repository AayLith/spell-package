﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public interface IGenericEffect
    {
        int getDuration ();

        void addToInspector ();

        void applyEffect ( Character perso );

        void startOfTurnEffect ( Character perso );

        void endOfTurnEffect ( Character perso );

        void onTakeDamages ( Character perso );

        void onDealDamages ( Character perso );
    }
}