﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class MightEffect : GenericEffect
    {
        public int amount;
        List<EffectSource> sources = new List<EffectSource> ();

        public override void applyEffect ( Character perso )
        {
            perso.addEffect ( this );
        }

        public MightEffect ( int duration , int amount , Character source , Character cible )
        {
            sources.Add ( new EffectSource ( source , amount , duration ) );
            this.amount = amount;
            applyEffect ( cible );
        }

        public void addSource ( int duration , int amount , Character source )
        {
            sources.Add ( new EffectSource ( source , amount , duration ) );
            this.amount += amount;
        }

        public override int mitigateDamages ( int damages )
        {
            return ( int ) ( ( float ) amount / 100 * damages );
        }
    }
}