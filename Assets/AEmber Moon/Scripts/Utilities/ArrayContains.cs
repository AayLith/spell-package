﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public static class ArrayContains
    {
        public static bool contains<T> ( T[] array , T value )
        {
            foreach ( T element in array )
            {
                if ( EqualityComparer<T>.Default.Equals ( element , value ) )
                    return true;
            }

            return false;
        }

        public static int containsTypeAmount<T> ( List<T> list , T value , int max )
        {
            int count = 0;
            foreach ( T element in list )
                if ( element.GetType () == value.GetType () )
                {
                    count++;
                    if ( count >= max )
                        return count;
                }
            return count;
        }

        public static int contains<T> ( List<T> list , Type type )
        {
            for ( int i = 0 ; i < list.Count ; i++ )
                if ( list[ i ].GetType () == type )
                    return i;
            return -1;
        }
    }
}