﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class IndicatorStrategySetter : MonoBehaviour
    {
        public SpellMetrics.indicatorsStrategies indicatorStrategy;

        IEnumerator Destroy ( IndicatorStrategy go )
        {
            yield return new WaitForEndOfFrame ();
            DestroyImmediate ( go );
        }

        private void Awake ()
        {
            Destroy ( this );
        }

#if UNITY_EDITOR
        private void OnValidate ()
        {
            if ( !UnityEditor.Selection.Contains ( gameObject ) )
                return;

            if ( GetComponent<IndicatorStrategy> () )
            {
                if ( GetComponent<IndicatorStrategy> ().GetType () == System.Type.GetType ( GetType ().Namespace + '.' + indicatorStrategy.ToString () ) )
                    return;
                UnityEditor.EditorApplication.delayCall += () =>
                    {
                        DestroyImmediate ( GetComponent<IndicatorStrategy> () );
                    };
            }

            if ( indicatorStrategy != SpellMetrics.indicatorsStrategies.Default )
            {
                gameObject.AddComponent ( System.Type.GetType ( GetType ().Namespace + '.' + indicatorStrategy.ToString () ) );
            }
        }
#endif
    }
}