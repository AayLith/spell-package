﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class StopParticleEmissions : MonoBehaviour
    {
        public float durationDeath;

        private void Start ()
        {
            Destroy ( gameObject , durationDeath );
        }
    }
}