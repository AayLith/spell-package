﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class EffectSource
    {
        Character source;
        int amount;
        int duration;

        public EffectSource ( Character source , int amount , int duration )
        {
            this.source = source;
            this.amount = amount;
            this.duration = duration;
        }
    } 
}
