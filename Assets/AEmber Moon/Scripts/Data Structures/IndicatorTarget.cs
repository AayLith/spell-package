﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public class IndicatorTarget
    {
        public GameObject target;
        public float distance;
        public byte indicatorIndex;

        public IndicatorTarget ( GameObject target , float distance , byte indicatorIndex )
        {
            this.target = target;
            this.distance = distance;
            this.indicatorIndex = indicatorIndex;
        }

        public IndicatorTarget ( KeyValuePair<GameObject , float> target , byte indicatorIndex )
        {
            this.target = target.Key;
            this.distance = target.Value;
            this.indicatorIndex = indicatorIndex;
        }
    }
}