﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorData
{
    public Quaternion rotation;
    public List<Vector3> positions = new List<Vector3> ();

    public IndicatorData ( List<Vector3> positions , Quaternion rotation )
    {
        this.rotation = rotation;
        this.positions.AddRange ( positions );
    }
}
