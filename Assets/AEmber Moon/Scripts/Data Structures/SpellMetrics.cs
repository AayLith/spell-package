﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    public static class SpellMetrics
    {
        public static float movePrecisionLimit = 0.05f;
        public enum damageType { Fire, Cold, Electric, Acid, Earth }
        public enum spellIndicators { Null, Area, Range, MinRange, Cone, Circle, Line, LineRenderer, CurvedLine, SimpleLine, SphereCollider }
        public enum indicatorsStrategies
        {
            Default,
            AreaIndicatorStrategy,                  // = "Area only"
            AreaStraightLineIndicatorStrategy,      // = "Area with straight line"
            AreaCurvedLineIndicatorStrategy,        // = "Area with curved line"
            BounceIndicatorStrategy,                // = "Line that connect with close targets"
            ConcentricAreaIndicatorStrategy,        // = "Area with concentric circles"
            ConcentricareaStraightLineStrategy,     // = "Area with concentric circles and straight line"
            ConcentricAreaCurvedLineStrategy,       // = "Area with concentric circles and curved line"
            ConeIndicatorStrategy,                  // = "Cones with fixed angle between them"
            ConeIndicatorAdaptativeStrategy,        // = "Cones with adaptative length and angle"
            ConeIndicatorSplitStrategy,             // = "Cones with adaptative angle between them"
            LineAreaSpellStrategy,                  // = "Line with adaptative length and an area on it's tip"
            LineIndicatorAdaptativeStrategy,        // = "Lines with adaptative angle between them"
            LineIndicatorStrategy,                  // = "Lines with fixed angle between them"
            ProjectileIndicatorStrategy,            // = "Lines that adapts it's length to it's closest target with fixed angle between them"
            ProjectileIndicatorAdaptativeStrategy,  // = "Lines that adapts it's length to it's closest target with adaptative angle between them"
            TargetedIndicatorStrategy               // = "Snaps to the closest target from the pointer"
        }

        public static float indicatorWallOffset = -0.05f;

        public static int obstacleLayer = 13;
    }
}