﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Æmber_Moon
{
    [System.Serializable]
    public class Damage
    {
        public int amount;
        public SpellMetrics.damageType type;
    }
}